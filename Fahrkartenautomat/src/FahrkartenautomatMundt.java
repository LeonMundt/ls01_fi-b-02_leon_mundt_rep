﻿
import java.util.Scanner;

class Fahrkartenautoma
{
    public static void main(String[] args)
    {
    	while(true)
    	{
    		Scanner tastatur = new Scanner(System.in);

    		double eingezahlterGesamtbetrag;       
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);   
    		eingezahlterGesamtbetrag = fahrkartenBezahlen(tastatur,zuZahlenderBetrag);
    		fahrkartenAusgeben(); 
    		rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);       
    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    							"vor Fahrtantritt entwerten zu lassen!\n"+
    							"Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
    	}
    }

	private static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag ;          // speichert den wert der für das Rückgeld berechnet wurde. Ist die Differenrz aus "eingezahlterGesamtbetrag" und "zuZahlenderBetrag". Wird auch benutzt um zu berechnen welche und wie viele Münzen ausgegeben werden.
		if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 1.995) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 0.995) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.495) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.195) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.095) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.045 )// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
		// TODO Auto-generated method stub
		
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
		// TODO Auto-generated method stub
		
	}

	private static double fahrkartenBezahlen(Scanner einwurf,double zuZahlenderBetrag) {
		// TODO Auto-generated method stub
		double eingezahlterGesamtbetrag = 0.0;
	    double eingeworfeneMünze;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f€ ",(zuZahlenderBetrag - eingezahlterGesamtbetrag)); 
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = einwurf.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
		return eingezahlterGesamtbetrag;
	}

	private static double fahrkartenbestellungErfassen(Scanner tastatur) {
		// TODO Auto-generated method stub
	       short anzahlTickets = 0;
	       short auswahl = 0; 
	       double einzelpreis = 100;
	       double ticketPreis = 0;
	       System.out.print("Fahrkartenbestellvorgang:\n"
	       					+ "========================\n\n");
	       
	       while(auswahl !=9) 
	       {
	    	   System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: \n"
	    			   + "\tEinzelfahrscheine (1)\n"
	    			   + "\tTageskarten (2)\n"
	    			   + "\tKleingruppen-Tageskarten (3)\n"
	    			   + "\tbezahlen(9)\n"
	    			   + "\n\nIhre Wahl:");

	    	   auswahl = tastatur.nextShort();
	       
	    	   while (auswahl < 1 || auswahl > 3 && auswahl != 9) {
	    		   System.out.print("Bitte geben Sie 1, 2 oder 3 ein:");
	    		   auswahl = tastatur.nextShort();
	    	   }

	    	   switch(auswahl)
	    	   {
	    	   case 1: 	einzelpreis = 2.90;
	       				break;
	    	   case 2: 	einzelpreis = 8.60;
  						break;
	    	   case 3: 	einzelpreis = 23.50;
  						break;
	    	   case 9: 	einzelpreis = 0;
  						break;
	    	   default: einzelpreis = 100;
  						break;
	    	   }
	       
	    	   if(auswahl != 9)
	    	   {
	    		   	System.out.print("Anzahl der Tickets: ");
	    	   		anzahlTickets = tastatur.nextShort(); //lesson learned --> Java is case sensitive
	    	   		while(anzahlTickets < 1 || anzahlTickets > 10)
	    	   		{
	    		   		System.out.print("Ungültige Eingabe. Versuchen Sie es nochmal und geben einen Wert zwischen 1 und 10 ein: \n");
	    		   		anzahlTickets = tastatur.nextShort();
	    	   		}
	    	   		ticketPreis += (einzelpreis * anzahlTickets);
	    	   }
	       }
		return ticketPreis;
	}
}