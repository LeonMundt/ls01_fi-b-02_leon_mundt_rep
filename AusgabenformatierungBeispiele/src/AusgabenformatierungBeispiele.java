
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		
		System.out.println("Hello");
		System.out.print("du \"Alex\"");
		
		System.out.printf("|%s|%n","123456789");
		System.out.printf("|%20s|\n","123456789");
		System.out.printf("|%-20s|","123456789");
		
		//Ganzzahl
		System.out.printf("|%d| %n", 123456789);
		System.out.printf("|%20d| %n", 123456789);
		System.out.printf("|%-20d| %n", 123456789);
		System.out.printf("|%-20d| %n", -123456789);
		System.out.printf("|%+-20d| %n", 123456789);
		System.out.printf("|%+020d| %n", 123456789);
		System.out.printf("|%d| %n", 123456789);

		
		System.out.printf("|%f| %n", 123456.789);
		System.out.printf("|%.2f| %n", 123456.789);
		System.out.printf("|%-20.2f| %n", 123456.789);
		System.out.printf("|%020.2f| %n", 123456.789);
		System.out.printf("|%+020.2f| %n", 123456.789);
	
	}


}
